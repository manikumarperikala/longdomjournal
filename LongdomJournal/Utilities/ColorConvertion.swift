//
//  ColorConvertion.swift
//  ConferenceSeries
//
//  Created by Omics on 11/20/19.
//  Copyright © 2019 Omics. All rights reserved.
//

import Foundation
import UIKit


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
func getLowestPrice(date:String)-> String
{
    let obj = UtilityManager(dateService: dateUtility!)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = kRequiredDateFormat
    let formatteddate = dateFormatter.date(from: date)
    let currentdate = dateFormatter.date(from: obj.getCurrentDate())
    let components = Set<Calendar.Component>([.day, .month, .year])
    let comp1 = Calendar.current.compare(currentdate!, to: formatteddate!, toGranularity: .day)
    let comp = Calendar.current.dateComponents(components, from: formatteddate!, to: currentdate!)
    print(comp)
    print(comp1)
   // year: 0 month: 0 day: -11 isLeapMonth: false
    return ""
}

func dateCompartion(startdate:String,enddate:String)-> String?
{
let startDate = startdate
let endDate = enddate
var year = ""
var month = ""
let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "yyyy-MM-dd"
if startDate != "0000-00-00" && endDate != "0000-00-00" && startDate != "" && endDate != ""
{
    let formatedStartDate = dateFormatter.date(from: startDate)!
    let formattedEndDate = dateFormatter.date(from: endDate)!
    let components = Set<Calendar.Component>([.day, .month, .year])
    let startdatecomp = Calendar.current.dateComponents(components, from: formatedStartDate)
    let enddatecomp = Calendar.current.dateComponents(components, from: formattedEndDate)
    if enddatecomp.year! > startdatecomp.year!
    {
        year = "\(enddatecomp.year!)"
    }
    else
    {
        year = "\(startdatecomp.year!)"
    }
    if enddatecomp.month! > startdatecomp.month!
    {
        let monthName = DateFormatter().shortMonthSymbols[enddatecomp.month! - 1]
        let monthName1 = DateFormatter().shortMonthSymbols[startdatecomp.month! - 1]
        month = monthName + " " + "\(enddatecomp.day!)" + "-" + monthName1 + "\(startdatecomp.day!)"
    }
    else
    {
        
        if (enddatecomp.month!) == (startdatecomp.month!)
        {
            let monthName = DateFormatter().shortMonthSymbols[startdatecomp.month! - 1]
            month = monthName + " " + "\(startdatecomp.day!)"  + " " + "-" + "\(enddatecomp.day!)"
        }
        
    }
        let finaldate = month + "," + " " + year
        return finaldate
}
 return "not fixed"

}
func iskeyexist(dict:NSDictionary,key:String) -> String
   {
       if let akey = dict[key]
       {
           return "\(akey)"
       }
       return ""
   }
enum VersionError:Error {
    case invalidBundleInfo, invalidResponse
    
  

}

   
func isUpdateAvailable() throws -> Bool {
    guard let info = Bundle.main.infoDictionary,
        let currentVersion = info["CFBundleShortVersionString"] as? String,
        let identifier = info["CFBundleIdentifier"] as? String,
        let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw VersionError.invalidBundleInfo
    }
    let data = try Data(contentsOf: url)
    guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
        throw VersionError.invalidResponse
    }
    print(json)
    if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
        print("version in app store", version,currentVersion);
        kAppStoreVersion = version
        return version != currentVersion
    }
    throw VersionError.invalidResponse
}
