//
//  currentCollectionViewCell.swift
//  LongdomJournal
//
//  Created by Mallesh Kurva on 31/08/20.
//  Copyright © 2020 Longdom. All rights reserved.
//

import UIKit

class currentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var art_type:UILabel!
    @IBOutlet weak var author_names:UILabel!
    @IBOutlet weak var title: UITextView!
    @IBOutlet weak var abstractBtn:UIButton!
    @IBOutlet weak var pdfBtn:UIButton!
    
    
}
