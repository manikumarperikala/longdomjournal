//
//  ViewController.swift
//  ConferenceSeries
//
//  Created by manikumar on 06/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import Kingfisher
import  WebKit

class HomeViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
   
   
    @IBOutlet weak var constantheight: NSLayoutConstraint!
    @IBOutlet weak var currentconstantheight: NSLayoutConstraint!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var currentissuecollectionview:UICollectionView!
    @IBOutlet weak var menuButton:UIBarButtonItem!
   
  
    var conferenceArray = NSMutableArray()
    var currentissueArray = NSMutableArray()
    var filteredConference = NSArray()
    var isfilter = String()
    var pageNumber: Int = 0
    var totalPages:Int  = 0
    var Norecords = -1
    var isLoading = false
    var filters = [String:Any]()
  
    var isLastpage = "false"
 

    override func viewDidLoad() {
        super.viewDidLoad()
      //  checkforupdate()
        if revealViewController() != nil
        {
             menuButton.target = revealViewController()
             menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
         view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
     
        self.title = "Home"
        self.pageNumber = 1
        self.totalPages = 1
        self.getConferences(params: ["page":"1"])
    }
    func checkforupdate()
    {
        DispatchQueue.global().async {
            do {
                let update = try isUpdateAvailable()
                
               
                DispatchQueue.main.async {
                    if update{
                        self.popupUpdateDialogue();
                    }
                    
                }
            } catch {
                print(error)
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
       return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let height = self.collectionView.collectionViewLayout.collectionViewContentSize.height
        constantheight.constant = height
        let height1 = self.currentissuecollectionview.collectionViewLayout.collectionViewContentSize.height
        currentconstantheight.constant = height1
      
        
        self.view.layoutIfNeeded()
    }
    //MARK:- CollectionView Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView ==  self.currentissuecollectionview
        {

            if currentissueArray.count > 0 {
                              currentissuecollectionview.backgroundView = nil
                            
                              return currentissueArray.count
            }
            else
            {
                let messageLabel = UILabel(frame: currentissuecollectionview.frame)
                messageLabel.text = "No data available."
                messageLabel.textAlignment = .center
                collectionView.backgroundView = messageLabel
            }
        }
        else
        {
            
        
        
        if conferenceArray.count > 0 {
                          collectionView.backgroundView = nil
                          return conferenceArray.count
        }
        else
        {
            let messageLabel = UILabel(frame: collectionView.frame)
            messageLabel.text = "No data available."
            messageLabel.textAlignment = .center
            collectionView.backgroundView = messageLabel
        }
        }
                      return 0
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        if collectionView == currentissuecollectionview
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1", for: indexPath) as! currentCollectionViewCell
            cell.layer.cornerRadius = 3.0
                   cell.layer.borderWidth = 2.0
                   cell.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0).cgColor
             let innerdict = currentissueArray[indexPath.row] as! NSDictionary
            
            cell.art_type.text = iskeyexist(dict: innerdict, key: "art_type")
            cell.author_names.text = iskeyexist(dict: innerdict, key: "author_names")
            cell.title.text = iskeyexist(dict: innerdict, key: "title")
            cell.abstractBtn.tag = indexPath.row
            cell.abstractBtn.addTarget(self, action: #selector(self.loadabstruct(_:)), for: .touchUpInside)
            cell.pdfBtn.tag = indexPath.row
            cell.pdfBtn.addTarget(self, action: #selector(self.loadpdf(_:)), for: .touchUpInside)
            
          return cell
        }
        else
        {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ContentCollectionViewCell
        cell.layer.cornerRadius = 3.0
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0).cgColor
        let innerdict = conferenceArray[indexPath.row] as! NSDictionary

        if (((innerdict.value(forKey: kiconurl) != nil)))
        {
            let img = (innerdict.value(forKey: kiconurl) as? String ?? "")
            if  ((img != "null" && img != "" && img != "<null>"))
            {
                let imgstr = kBaseUrl + "/assets/category-images/" + img
                let imgurl = URL(string: imgstr)
                 cell.IconImg.kf.setImage(with: imgurl!)

            }
            else
                
            {
                cell.IconImg.image = UIImage(imageLiteralResourceName: kimage_not_available)
            }
        }
        cell.titleLbl.text =  iskeyexist(dict: innerdict, key: ktitle)
            return cell
        }
        return cell
        
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
         if collectionView == currentissuecollectionview
         {
            return 10.0
        }
           return 10
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == currentissuecollectionview
        {
            return 1.0
        }
        return 1.0
    }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == currentissuecollectionview
        {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
        let height:CGFloat = 150
        if collectionView == currentissuecollectionview
        {
            
            let width = self.currentissuecollectionview.frame.size.width
            
             var height: CGFloat = 0
            
               //we are just measuring height so we add a padding constant to give the label some room to breathe!
                var padding: CGFloat = 135
            
                //estimate each cell's height
            let dict = (currentissueArray[indexPath.row] as! NSDictionary)
            let title = iskeyexist(dict: dict, key: "title")
           
            let value = estimateFrameForText(text: title)
            height = value.height + padding
          
           
                return CGSize(width: width-10, height: height)        }
        else
        {
            if UI_IDIOM == .pad
             {
                 //height = collectionView.frame.width / 3 - 10
                 return CGSize(width: collectionView.frame.width / 3 - 10, height: height)
            
             }else{
                 return CGSize(width: collectionView.frame.width / 2 - 10, height: height)

             }
        }
        
        
       }
    
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let innerdict = conferenceArray[indexPath.row] as? NSDictionary ?? [:]
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier:"category") as! CategoryListViewController
            vc.conferenceDict = innerdict
            self.navigationController?.pushViewController(vc, animated: false)
        }
    
    // Load Abstruct
    
    @objc func loadabstruct(_ sender:UIButton)
    {
        print(sender.tag)
       let dict = self.currentissueArray[sender.tag] as! NSDictionary
        let path = iskeyexist(dict: dict, key: "abstractlink")
        if path != "" && path != "null"
        {
               let obj = NetworkManager(utility: networkUtility!)
               let params = ["abstractlink":path]
               self.view.addActivity()
               obj.postRequestApiWith(api: kLongdomabstractdplyapi, params: params){ (response) in
               if response != nil
               {
                   if response is NSDictionary
                   {
                       let dict = response as! NSDictionary
                       let status = iskeyexist(dict: dict, key: kstatus)
                  
                       if status == "1" || status == "true"
                       {
                              // self.abt_journal_detail = iskeyexist(dict: dict, key: "abt_journal_details")
                              // self.loaddata(abt_journal: self.abt_journal_detail)
                       }
                       else
                       {
                        //   self.abt_journal_detail = "No data available"
                      //     self.loaddata(abt_journal: self.abt_journal_detail)
                           
                       }
                           DispatchQueue.main.async {
                               self.view.removeActivity()
                               
                           }
                   }
                   
               }
               else
               {
                //   self.abt_journal_detail = "No data available"
               //    self.loaddata(abt_journal: self.abt_journal_detail)
                   DispatchQueue.main.async {
                       self.view.removeActivity()
                   }
               }
                   
              }
        }
 
    }
func loaddata(abt_journal:String)
{
 //   self.detailedtxt.attributedText = abt_journal.htmlToAttributedString
}
    @objc func loadpdf(_ sender:UIButton)
    {
        let dict = self.currentissueArray[sender.tag] as! NSDictionary
        let path = iskeyexist(dict: dict, key: "pdflink")
        if path != "" && path != "null"
        {
            let destinationURL = URL(string: path)
            DispatchQueue.main.async {
                                  
              let data = try! Data(contentsOf: destinationURL!)
              let loadwebview = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
                loadwebview.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: destinationURL!.deletingLastPathComponent())
              let docVC = UIViewController()
              docVC.view.addSubview(loadwebview)
              docVC.view.addActivity()
              
              loadwebview.translatesAutoresizingMaskIntoConstraints = false
              loadwebview.leftAnchor.constraint(equalTo: docVC.view.leftAnchor).isActive = true
              loadwebview.rightAnchor.constraint(equalTo: docVC.view.rightAnchor).isActive = true
              loadwebview.topAnchor.constraint(equalTo: docVC.view.topAnchor).isActive = true
              loadwebview.bottomAnchor.constraint(equalTo: docVC.view.bottomAnchor).isActive = true
              docVC.title = destinationURL?.lastPathComponent
              self.navigationController?.pushViewController(docVC, animated: true)
              docVC.view.removeActivity()
                              }
        }
        
    }
    private func estimateFrameForText(text: String) -> CGRect {
        //we make the height arbitrarily large so we don't undershoot height in calculation
        let height: CGFloat = 0
    
        let size = CGSize(width: self.view.frame.size.width-10, height: height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSAttributedString.Key.font: UIFont(name: kAppFontRegular, size: 14.0)]
        return NSString(string: text).boundingRect(with: size, options: options, attributes: attributes, context: nil)
    }
 
    
    //MARK:- To get Conferences list
    func getConferences(params:[String:Any])
    {
        if self.pageNumber == 1
        {
             self.view.addActivity()
        }
       
        let obj = NetworkManager(utility: networkUtility!)
        let params = params
        obj.postRequestApiWith(api: kLongdomHomeapi, params: params){ (response) in
        if response != nil
        {
            if response is NSDictionary
            {
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
           
                if status == "1" || status == "true"
                {
                    
                    if (dict.value(forKey: kconferences) != nil)
                    {
                        if dict.value(forKey: kconferences) is NSArray
                        {
                            let confarr = dict.value(forKey: kconferences) as? NSArray ?? []
                            for dict in confarr
                            {
                                self.conferenceArray.add(dict)
                            }
                            
                        }
                    }
                  

                    if (dict.value(forKey: "currissue_highlights") != nil)
                      {
                          if dict.value(forKey: "currissue_highlights") is NSArray
                          {
                              let confarr = dict.value(forKey: "currissue_highlights") as? NSArray ?? []
                              for dict in confarr
                              {
                                self.currentissueArray.add(dict)
                              }
                              
                          }
                  }
                    
                }
                else
                {
                    print("status failed")
                }
                if self.conferenceArray.count > 0
                {
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        self.collectionView.reloadData()
                        self.isLoading = false
                    }
                }
                    if self.currentissueArray.count > 0
                                   {
                                       DispatchQueue.main.async {
                                           self.view.removeActivity()
                                           self.currentissuecollectionview.reloadData()
                                           self.isLoading = false
                                       }
                                   }
                else
                {
                    self.pageNumber = self.Norecords
                }
            }
            
        }
        else
        {
           
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
        }
            
       }
    }
    

    @IBAction func filterAction(_ sender: UIBarButtonItem) {
      /*  let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"filter") as! FiltersViewController
        vc.filterdelegate = self
        self.navigationController?.pushViewController(vc, animated: false)*/
        
    }
      
        func insertRowAtBottom() {
            if pageNumber != Norecords {
                pageNumber += 1
                if isfiltersapplied == "false"
                {
                    if pageNumber <= totalPages
                    {
                        isLastpage = "false"
                        getConferences(params: ["filters":isfiltersapplied,"page":pageNumber])
                        
                    }
                    else
                    {
                        isLoading = false
                        isLastpage = "true"
                    }
                }
                else
                {
                    if pageNumber <= totalPages
                   {
                    self.filters.updateValue(pageNumber, forKey: "page")
                    self.getConferences(params: self.filters)
                   }
                    else
                    {
                        isLoading = false
                        isLastpage = "true"
                    }
                
                }
                
               
            }
        }
    @objc func myTargetFunction()
    {
        
       /* let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"filter") as! FiltersViewController
        self.navigationController?.pushViewController(vc, animated: false)*/
    }
    func filterData(conferencearr: [String:Any], isfilter: String) {
        self.filters = conferencearr
//        self.getConferences(params: self.filters)
      
     }
    func popupUpdateDialogue(){
           var versionInfo = kAppStoreVersion
           
           let alertMessage = "A new version of Conference Series Application is available,Please update to version "+versionInfo;
        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertController.Style.alert)
     
           let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
               if let url = URL(string: kAppstoreLink),
                   UIApplication.shared.canOpenURL(url){
                   if #available(iOS 10.0, *) {
                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
           })
           let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
           })
           alert.addAction(okBtn)
           alert.addAction(noBtn)
           self.present(alert, animated: true, completion: nil)
           
       }
} 
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        var attrstr = NSMutableAttributedString()
        do {
            return  try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            
          //  let textRangeForFont : NSRange = NSMakeRange(0, attrstr.length)
          //  attrstr.addAttributes([NSAttributedString.Key.font : UIFont(name: kAppFontRegular, size:13.0)!], range: textRangeForFont)
         //   attrstr.addAttributes([NSAttributedString.Key.foregroundColor:UIColor.gray], range: textRangeForFont)
            } catch {
            return NSAttributedString()
        }
   //     return attrstr
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
class CustomTextField1:UITextField
{
    var imageIconView = UIImageView()
    var paddingview = UIView()
    let padding = 8
    let size = 20
    override func awakeFromNib()
    {
        super.awakeFromNib()
        paddingview = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size))
        imageIconView = UIImageView(frame: CGRect(x: 10, y:paddingview.frame.size.height/2-(10), width: 20, height: 20))
        paddingview.addSubview(imageIconView)
        self.leftViewMode = .always
        self.leftView = paddingview
    }
    
    func setPaddingWithIcon(image:UIImage)
    {
        imageIconView.image = image
    }
    
    deinit {
        
    }
}
