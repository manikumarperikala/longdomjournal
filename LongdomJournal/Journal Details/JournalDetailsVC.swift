//
//  JournalDetailsVC.swift
//  LongdomJournal
//
//  Created by Mallesh Kurva on 02/09/20.
//  Copyright © 2020 Longdom. All rights reserved.
//

import UIKit

class JournalDetailsVC: UIViewController {
    
    @IBOutlet weak var detailedtxt:UITextView!
    var pageurl = String()
    var pagetitle = String()
    var abt_journal_detail = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = pagetitle
        self.getaboutjournal(pageurl:pageurl)
        
    }
    
    func getaboutjournal(pageurl:String)
    {
        let obj = NetworkManager(utility: networkUtility!)
        let params = ["page_url":pageurl]
        self.view.addActivity()
        obj.postRequestApiWith(api: kLongdomaboutjournalapi, params: params){ (response) in
        if response != nil
        {
            if response is NSDictionary
            {
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
           
                if status == "1" || status == "true"
                {
                        self.abt_journal_detail = iskeyexist(dict: dict, key: "abt_journal_details")
                        self.loaddata(abt_journal: self.abt_journal_detail)
                }
                else
                {
                    self.abt_journal_detail = "No data available"
                    self.loaddata(abt_journal: self.abt_journal_detail)
                    
                }
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        
                    }
            }
            
        }
        else
        {
            self.abt_journal_detail = "No data available"
            self.loaddata(abt_journal: self.abt_journal_detail)
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
        }
            
       }
    }
    
    func loaddata(abt_journal:String)
    {
        self.detailedtxt.attributedText = abt_journal.htmlToAttributedString
    }

}
