//
//  CurrentissueCollectionViewCell.swift
//  LongdomJournal
//
//  Created by Mallesh Kurva on 02/09/20.
//  Copyright © 2020 Longdom. All rights reserved.
//

import UIKit

class CurrentissueCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var art_type:UILabel!
    @IBOutlet weak var author_names:UILabel!
    @IBOutlet weak var title: UITextView!
    @IBOutlet weak var doi:UILabel!
    @IBOutlet weak var abstractBtn:UIButton!
    @IBOutlet weak var pdfBtn:UIButton!
    @IBOutlet weak var fulltextBtn:UIButton!
    @IBOutlet weak var publisheddate:UILabel!
    
}
