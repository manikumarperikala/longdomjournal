//
//  InpressVC.swift
//  LongdomJournal
//
//  Created by Mallesh Kurva on 02/09/20.
//  Copyright © 2020 Longdom. All rights reserved.
//

import UIKit

class InpressVC:UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
     @IBOutlet weak var currentissuecollectionview:UICollectionView!
     var currentissueArray = NSMutableArray()
     var conferenceDict = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
       
    
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
  
        let params = ["journalcode":iskeyexist(dict: conferenceDict, key: "journalcode"),"rel_keyword":iskeyexist(dict: conferenceDict, key: "rel_keyword"),"journal_logo":iskeyexist(dict: conferenceDict, key: "journal_logo")]
        self.getissuelist(params:params)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
               if currentissueArray.count > 0 {
                                 currentissuecollectionview.backgroundView = nil
                                 return currentissueArray.count
               }
               else
               {
                   let messageLabel = UILabel(frame: currentissuecollectionview.frame)
                   messageLabel.text = "No data available."
                   messageLabel.textAlignment = .center
                   collectionView.backgroundView = messageLabel
               }
           
                return 0
          }
          
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1", for: indexPath) as! CurrentissueCollectionViewCell
            cell.contentView.layer.cornerRadius = 4.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = false
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
            cell.layer.shadowRadius = 4.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
//               cell.layer.cornerRadius = 3.0
//                      cell.layer.borderWidth = 2.0
//                      cell.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0).cgColor
                let innerdict = currentissueArray[indexPath.row] as! NSDictionary
               
               cell.art_type.text = iskeyexist(dict: innerdict, key: "art_type")
               cell.author_names.text = iskeyexist(dict: innerdict, key: "author_names")
               cell.title.text = iskeyexist(dict: innerdict, key: "title")
               cell.doi.text = "DOI:" + iskeyexist(dict: innerdict, key: "doi_num")
            cell.publisheddate.text = "Published Date:\(iskeyexist(dict: innerdict, key: "pub_date"))" + "\n" + "Received Date:\(iskeyexist(dict: innerdict, key: "rec_date"))"
           return cell
           
          }
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            if collectionView == currentissuecollectionview
            {
               return 10.0
           }
              return 10
          }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           if collectionView == currentissuecollectionview
           {
               return 1.0
           }
           return 1.0
       }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           if collectionView == currentissuecollectionview
           {
               return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
           }
           return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
    
               let width = self.currentissuecollectionview.frame.size.width
               
                var height: CGFloat = 0
               
                  //we are just measuring height so we add a padding constant to give the label some room to breathe!
                   let padding: CGFloat = 250
               
                   //estimate each cell's height
               let dict = (currentissueArray[indexPath.row] as! NSDictionary)
               let title = iskeyexist(dict: dict, key: "title")
               //let author = iskeyexist(dict: dict, key: "author_names")
            
              
               let value = estimateFrameForText(text: title)
               //let value1 =  estimateFrameForText(text: author)
               height = value.height + padding
             
              
             return CGSize(width: width-10, height: height)
          
           
           
          }
       
          func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//               let innerdict = conferenceArray[indexPath.row] as? NSDictionary ?? [:]
//               let storyBoard = UIStoryboard(name: kMain, bundle: nil)
//               let vc = storyBoard.instantiateViewController(withIdentifier:"category") as! CategoryListViewController
//               vc.conferenceDict = innerdict
//               self.navigationController?.pushViewController(vc, animated: false)
           }
       private func estimateFrameForText(text: String) -> CGRect {
           //we make the height arbitrarily large so we don't undershoot height in calculation
           let height: CGFloat = 0
       
           let size = CGSize(width: self.view.frame.size.width-10, height: height)
           let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
           let attributes = [NSAttributedString.Key.font: UIFont(name: kAppFontRegular, size: 14.0)]
           return NSString(string: text).boundingRect(with: size, options: options, attributes: attributes, context: nil)
       }
    
    func getissuelist(params:[String:String])
    {
        let obj = NetworkManager(utility: networkUtility!)
        
        self.view.addActivity()
        obj.postRequestApiWith(api:kLongdominpressapi, params: params){ (response) in
        if response != nil
        {
            if response is NSDictionary
            {
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
           
                if status == "1" || status == "true"
                {
                    
                    if (dict.value(forKey: "inpress_details") != nil)
                    {
                        if dict.value(forKey: "inpress_details") is NSArray
                        {
                            let confarr = dict.value(forKey: "inpress_details") as? NSArray ?? []
                            for dict in confarr
                            {
                                self.currentissueArray.add(dict)
                            }
                            
                        }
                    }
                    
                }
                else
                {
                    print("status failed")
                }
                if self.currentissueArray.count > 0
                {
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        self.currentissuecollectionview.reloadData()
                        
                    }
                }
            }
            
        }
        else
        {
           
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
        }
            
       }
    }
    
}
