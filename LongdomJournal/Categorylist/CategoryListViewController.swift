//
//  CategoryListViewController.swift
//  LongdomJournal
//
//  Created by Mallesh Kurva on 01/09/20.
//  Copyright © 2020 Longdom. All rights reserved.
//

import UIKit

class CategoryListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    var conferenceDict = NSDictionary()
    var categoryArray = NSMutableArray()
    
    @IBOutlet weak var cattableview:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let catid = iskeyexist(dict: conferenceDict, key: "cat_id")
        self.title = iskeyexist(dict: conferenceDict, key: "cat_name")
       // self.cattableview.separatorColor = UIColor.init(white: 0.95, alpha: 1.0)
        self.getCategoryList(catid:catid)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if categoryArray.count > 0 {
                                    cattableview.backgroundView = nil
                                  
                                    return categoryArray.count
                  }
                  else
                  {
                      let messageLabel = UILabel(frame: cattableview.frame)
                      messageLabel.text = "No data available."
                      messageLabel.textAlignment = .center
                      cattableview.backgroundView = messageLabel
                  }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CategoryCell
//        cell.contentView.layer.cornerRadius = 4.0
//        cell.contentView.layer.borderWidth = 1.0
//        cell.contentView.layer.borderColor = UIColor.clear.cgColor
//        cell.contentView.layer.masksToBounds = false
//        cell.layer.shadowColor = UIColor.gray.cgColor
//        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
//        cell.layer.shadowRadius = 4.0
//        cell.layer.shadowOpacity = 1.0
//        cell.layer.masksToBounds = false
//        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
//        cell.layer.cornerRadius = 3.0
//        cell.layer.borderWidth = 2.0
//        
//        cell.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0).cgColor
        
        let innerdict = categoryArray[indexPath.section] as! NSDictionary

        if (((innerdict.value(forKey: "flyerimg") != nil)))
        {
            let img = (innerdict.value(forKey: "flyerimg") as? String ?? "")
            if  ((img != "null" && img != "" && img != "<null>"))
            {
                let imgstr = img
                let imgurl = URL(string: imgstr)
                 cell.catImg.kf.setImage(with: imgurl!)

            }
            else
            {
                cell.catImg.image = UIImage(imageLiteralResourceName: kimage_not_available)
            }
        }
        cell.journal.text = iskeyexist(dict: innerdict, key: "journal")
        cell.volume.text = iskeyexist(dict: innerdict, key: "vol_issue_name")
        
        return cell
    
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let innerdict = categoryArray[indexPath.section] as? NSDictionary ?? [:]
        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"catdetail") as! CategoryDetails
        vc.catdetails = innerdict
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func getCategoryList(catid:String)
    {
       
        let obj = NetworkManager(utility: networkUtility!)
        let params = ["cat_id":catid]
        self.view.addActivity()
        obj.postRequestApiWith(api: kLongdomCategorylistapi, params: params){ (response) in
        if response != nil
        {
            if response is NSDictionary
            {
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
           
                if status == "1" || status == "true"
                {
                    
                    if (dict.value(forKey: "subcat_details") != nil)
                    {
                        if dict.value(forKey: "subcat_details") is NSArray
                        {
                            let confarr = dict.value(forKey: "subcat_details") as? NSArray ?? []
                            for dict in confarr
                            {
                                self.categoryArray.add(dict)
                            }
                            
                        }
                    }
                    
                }
                else
                {
                    print("status failed")
                }
                if self.categoryArray.count > 0
                {
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        self.cattableview.reloadData()
                        
                    }
                }
            }
            
        }
        else
        {
           
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
        }
            
       }
    }

}
