//
//  CategoryDetails.swift
//  LongdomJournal
//
//  Created by Mallesh Kurva on 01/09/20.
//  Copyright © 2020 Longdom. All rights reserved.
//

import UIKit

class CategoryDetails: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var cattableview:UITableView!
    
    var catdetails = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = iskeyexist(dict: catdetails, key: "journal")
        

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return categoryarr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.layer.cornerRadius = 3.0
        cell.layer.borderWidth = 2.0
        
        cell.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0).cgColor
        
        let innerdict = categoryarr[indexPath.section]
        (cell.viewWithTag(10) as! UILabel).text = "\(innerdict)"
        return cell
    
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  let categoryarr = ["Journal Home","In Press","Current Issue","Archive","Submit ManuScript","Instructions for Authors","Special Issues","Contact"]
        let selectedindex = categoryarr[indexPath.section]
        switch selectedindex {
        case "Journal Home":
            let pageurl = iskeyexist(dict: catdetails, key: "home_url")
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier:"JDVC") as! JournalDetailsVC
            vc.pageurl = pageurl
            vc.pagetitle = iskeyexist(dict: catdetails, key: "journal")
            self.navigationController?.pushViewController(vc, animated: false)
            break
        case "Current Issue":
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier:"CIVC") as! CurrentissueVC
            vc.conferenceDict = catdetails
            self.navigationController?.pushViewController(vc, animated: false)
            break
        case "In Press":
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier:"IPVC") as! InpressVC
            vc.conferenceDict = catdetails
            self.navigationController?.pushViewController(vc, animated: false)
            break
        default:
            break
        }
        
    }
    

}
