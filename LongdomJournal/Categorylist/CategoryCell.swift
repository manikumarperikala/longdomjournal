//
//  CategoryCell.swift
//  LongdomJournal
//
//  Created by Mallesh Kurva on 01/09/20.
//  Copyright © 2020 Longdom. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var catImg:UIImageView!
    @IBOutlet weak var journal:UILabel!
    @IBOutlet weak var volume:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
